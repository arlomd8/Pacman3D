﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonEvent : MonoBehaviour
{
    public void toGame()
    {
        Application.LoadLevel("Main");
    }

    public void toCredit()
    {
        Application.LoadLevel("Credit");
    }

    public void toBack()
    {
        Application.LoadLevel("Menu");
    }

    public void toQuit()
    {
        Application.Quit();
    }
}
