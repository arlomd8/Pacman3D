﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public Transform target;
    public Text winText;
    private NavMeshAgent navComponent;

    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        navComponent = this.gameObject.GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if(winText.text != "YOU LOSE!" && winText.text != "YOU WIN!")
        {
            if (target)
            {
                navComponent.SetDestination(target.position);
            }
            else
            {
                target = this.gameObject.GetComponent<Transform>();
            }
        }
        else
        {
            target = this.gameObject.GetComponent<Transform>();
        }
    }
}
