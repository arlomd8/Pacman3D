﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerController : MonoBehaviour
{
    public float speed;
    private Rigidbody rb;
    public Text countText;
    public Text winText;
    public Text lifeText;
    private int count;
    private int life;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        life = 3;
        lifeText.text = "LIFE : " + life;
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (count % 5 == 0 )
            {
                this.gameObject.GetComponent<SphereCollider>().isTrigger = true;
            }
            else
                this.gameObject.GetComponent<SphereCollider>().isTrigger = false;
        }

        if(this.gameObject.transform.position.y == -2)
        {
            life = 0;
            SetCountText();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count += 1;
            SetCountText();
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            life -= 1;
            SetCountText();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            life -= 1;
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "COUNT : " + count.ToString();
        lifeText.text = "LIFE : " + life.ToString();
        if(count >= 20)
        {
            winText.text = "YOU WIN!";
        }

        if(life <= 0)
        {
            life = 0;
            winText.text = "YOU LOSE!";
        }

    }
}
